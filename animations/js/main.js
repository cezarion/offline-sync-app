$('#scene').on('click', 'a',function(event) {
    var rubrique = $(this).attr('data-rubrique');
    var self = $(this);
    $('.zone').removeClass('dezoom', 'defly');

    $(this).addClass('zoom');
    $('.zone:not(#zone-0)').not(self).addClass('fly');

    $(this).on('webkitAnimationEnd oanimationend msAnimationEnd animationend',
        function(e) {


        //$('#page').load('content.html');

        $('body').removeClass().addClass(rubrique);
        $('.slidingdoor').removeClass('hide').addClass('opening');
        $('#page').addClass('on');
        $('.zone').removeClass('zoom fly');
        $('.navbar, .content').removeClass('hide');
    });
    return false;
});


$('#page .navbar a').on('click', function(event) {
    event.preventDefault();
    var target_back = $('body').attr('class');

    $('.slidingdoor').removeClass('opening').addClass('closing');

    $('.slidingdoor').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
        function(e) {

        $(this).addClass('hide');

        $('.zone').each(function(index, val) {
            var rubrique = $(this).attr('data-rubrique');
            if (rubrique == target_back) {
                $(this).addClass('dezoom');
                $('.zone:not(#zone-0)').addClass('defly');
            }
        });

        $('.navbar, .content').addClass('hide');

        if (!$('body').hasClass('violet')) {
            $('#page').removeClass('on');
            $('.slidingdoor').removeClass('closing');
        };
    });


    if ($('body').hasClass('violet')) {
        $('.content').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
            function(e) {
                $('#page').removeClass('on');
                $('.slidingdoor').removeClass('closing');
        });
    };
});