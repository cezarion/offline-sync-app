/*
* @Author: Cezarion
* @Date:   2014-05-23 16:44:25
* @Last Modified by:   Cezarion
* @Last Modified time: 2014-05-27 16:10:00
*/
angular.module('starter.directives', ['starter.config', 'ngAnimate', 'angularMoment'])
