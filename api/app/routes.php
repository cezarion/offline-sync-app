<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    return View::make('hello');
});


//Route::model('product', 'Product');
Route::get('products/{id?}', function( $id = null )
{
    if( is_null($id)) {
        return Product::with('image' , 'demo' , 'booklet' , 'video' )->get();
    }
    return Product::with('image' , 'demo' , 'booklet' , 'video' )->find($id);
});

Route::pattern('id', '[0-9]+');
Route::get('attachments/{id}', function( $id = null )
{
    return Media::find($id);
});

Route::get('attachments', function()
{
   if( Input::input('updated-since') ) {
        $updated_at = Input::input('updated-since');
        return Media::where('updated_at', '>=', urldecode($updated_at))->get();
   }

   return Media::all();

});

// Route::get('attachments/updated-since/{updated_at}', function( $updated_at = null )
// {
//     return Media::where('updated_at', '>=', urldecode($updated_at))->get();
// });
