/*
* @Author: Cezarion
* @Date:   2014-05-13 12:55:03
* @Last Modified by:   Cezarion
* @Last Modified time: 2014-05-14 17:00:32
*/
angular.module('starter.database', ['starter.config'])
// DB wrapper
.factory('DB', function($q, DB_CONFIG) {
    var self = this;
    self.db = null;

    self.initDatabase = function(){
        // persistence.store.cordovasql.config(
        //   persistence,
        //   DB_CONFIG.name,
        //   DB_CONFIG.version,       // DB version
        //   DB_CONFIG.displayName,   // DB display name
        //   DB_CONFIG.size,          // DB size
        //   0                        // SQLitePlugin Background processing disabled
        // );
        //clientDb = new Lawnchair{ name: DB_CONFIG.name };
    },

    self.init = function() {
        //self.db = window.sqlitePlugin.openDatabase({name: DB_CONFIG.name }); //in production
        self.db = window.openDatabase(DB_CONFIG.name, '1.0', 'database', -1);

        angular.forEach(DB_CONFIG.tables, function(table) {
            var columns = [];

            angular.forEach(table.columns, function(index,type) {
                columns.push(index + ' ' + type);
            });

            var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
            self.query(query);
            console.log('Table ' + table.name + ' initialized');
        });
    };

    self.query = function(query, bindings) {
        bindings = typeof bindings !== 'undefined' ? bindings : [];
        var deferred = $q.defer();

        self.db.transaction(function(transaction) {
            transaction.executeSql(query, bindings, function(transaction, result) {
                deferred.resolve(result);
            }, function(transaction, error) {
                deferred.reject(error);
            });
        });

        return deferred.promise;
    };

    self.fetchAll = function(callback) {
        var output = [];

        for (var i = 0; i < result.rows.length; i++) {
            output.push(result.rows.item(i));
        }

        return output;
    };

    self.fetch = function(callback) {
        return result.rows.item(0);
    };



    return self;
})
.factory('RestApi', function($http)
{
    return {
        load : function(link){
            return $http({
                method: 'GET',
                url: link
            });
        }
    }
})
// Resource service example
.factory('ProductModel', function(DB , DB_CONFIG ) {
    var self = this,
        datas = {},
        Product = persistence.define('Product', DB_CONFIG.tables.products.columns);

        persistence.schemaSync();

    return {
        add: function(datas){
            var p = new Product(datas);
            persistence.add(p);
            persistence.flush();

            return Product;
        },
        all: function()
        {
            // return DB.query('SELECT * FROM products')
            //          .then(function(result){
            //             return DB.fetchAll(result);
            //         });
            return Product.all();
        },

        get: function(friendId)
        {
          // Simple index lookup
            return DB.query('SELECT * FROM products WHERE id = ?', [id])
                     .then(function(result){
                        return DB.fetch(result);
                    });
        }
    }
})
// Resource service medias
.factory('Medias', function(DB) {
    var self = this;

    self.all = function() {
        return DB.query('SELECT * FROM medias')
        .then(function(result){
            return DB.fetchAll(result);
        });
    };

    self.getById = function(id) {
        return DB.query('SELECT * FROM medias WHERE id = ?', [id])
        .then(function(result){
            return DB.fetch(result);
        });
    };

    return self;
});