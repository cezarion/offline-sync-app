<?php

class Step extends \Eloquent {
    protected $fillable = ['name'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'steps';


    public function product()
    {
        return $this->belongsTo('Products');
    }

}
