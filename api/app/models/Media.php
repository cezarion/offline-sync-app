<?php

class Media extends \Eloquent {

    protected $fillable = [];

    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'medias';

    public function products()
    {
        return $this->hasMany('Product');
    }

}
