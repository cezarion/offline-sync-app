angular.module('starter.controllers', [])

.controller('DashCtrl', [ '$scope', function($scope) {
    // $scope.items = [
    //     { id: 0, name: 'CopySud',rubrique: 'white'},
    //     { id: 1, name: 'text 1', rubrique: 'jaune'},
    //     { id: 2, name: 'text 2', rubrique: 'rouge'},
    //     { id: 3, name: 'text 3', rubrique: 'bleu'},
    //     { id: 4, name: 'text 4', rubrique: 'violet'},
    //     { id: 5, name: 'text 5', rubrique: 'noir'}
    // ];
    $scope.items = [
        { id: 0, name: 'CopySud',rubrique: 'white'},
        { id: 1, name: 'text 1', rubrique: 'jaune'},
        { id: 2, name: 'text 2', rubrique: 'rouge'},
        { id: 3, name: 'text 3', rubrique: 'bleu'},
        { id: 4, name: 'text 4', rubrique: 'violet'},
        { id: 5, name: 'text 5', rubrique: 'noir'}
    ];
}])
.directive('menuItem', ['$document', function($document) {
    return {
      //restrict: 'E',
      link: function(scope, element, attrs, DashCtrl) {
        console.log(element);
        element.on('click',function(event){
            var self = this;
            $('.zone').removeClass('dezoom', 'defly');
        })
      },
      template: '<span>{{item.name}}</span>',

    };

  }])

.controller('FriendsCtrl', function($scope, Friends) {
  $scope.friends = Friends.all();
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
  $scope.friend = Friends.get($stateParams.friendId);
})

.controller('AccountCtrl', function($scope) {
})

.controller('ProductsCtrl', function($scope, MediasManager ) {


    $scope.download = function(){
        MediasManager.download('http://offline-sync.node/api/public/uploads/01-backup-moto-prod-reminder-2W-2014-05-15-10-10.sql');
    }
    $scope.list = function(){
        $scope.files = MediasManager.list();
    }

    // Get all the documents
    // Product.all().then(function(Products){
    //      $scope.products = products;
    // });
    //ProductModel.sync();
    // Get one Product, example with id = 2
    // Product.getById(2).then(function(Product) {
    //     $scope.product = product;
    // });
})

.directive('myFiles' , ['$compile','MediasManager' , function($compile, MediasManager){
    return {
        link: {
            post: function postLink(scope, element, attrs) {
                    scope.files = MediasManager.listFiles().then(function(results){
                        console.log(results);
                        return results;
                    }, function(error){
                        console.log(error);
                    });
                    console.log('popo');
                }
        },
        template: 'File : {{file}}'
    };
}])

.controller('ProductDetailCtrl', function($scope, $rootScope, $stateParams ) {

})
.controller('DatabaseSyncCtrl', function($scope, $log, $q, Medias ) {

    //Medias.initialize();

    $scope.medias = [];
    $scope.media = null;
    $scope.messages = Medias.messages;
    $scope.loading = Medias.loading;

    $scope.reset = function(){
        Medias.reset();
    }

    $scope.clear = function(){
        Medias.clear();
    }

    $scope.sync = function(){
        Medias.sync();
    }

    $scope.$watch(
        function(){ return Medias.messages },

        function(newVal) {
          $scope.messages = newVal;
        }
      );

    $scope.$watch(
        function(){ return Medias.loading },

        function(newVal) {
          console.log('loading : ' + newVal);
          $scope.loading = newVal;
        }
      );

    // Get all the documents
    // Medias.all().then(function(medias){
    //     $scope.medias = medias;
    // });
    // Get one document, example with id = 2
    // Medias.getById(2).then(function(document) {
    //     $scope.document = document;
    // });

});
