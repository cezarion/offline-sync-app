/*
* @Author: Cezarion
* @Date:   2014-05-13 12:43:49
* @Last Modified by:   Cezarion
* @Last Modified time: 2014-05-20 10:02:32
*/
angular.module('starter.config', [])
.constant('APP_CONFIG', {
    dbName: 'mobileApp',
    displayName: 'Client App DB',
    version: '1.0',
    size: 5 * 1024 * 1024,
    api_base: 'http://offline-sync.node/api/public',
    tables:
    {
       products : {
            name: 'products',
            columns: {
                //id:        'integer primary key not null',
                title:     'text',
                name:      'text',
                title:     'text',
                content:   'text',
                image_id:  'int' ,
                demo_id:   'int' ,
                booklet_id:'int' ,
                video_id:  'int' ,
                created_at:'date',
                updated_at:'date'
            },
            url: 'http://offline-sync.node/api/public/products'
        },
        medias : {
            name: 'medias',
            columns: {
                id:          'integer primary key not null',
                filename:    'text',
                filesize:    'text',
                mimetype:    'text',
                name:        'text',
                description: 'text',
                created_at:  'date',
                updated_at:  'date'
            },
            url: 'http://offline-sync.node/api/public/attachments'
        },
        syncHistory : {
            name: 'syncHistory',
            columns: {
                id:          'integer primary key not null',
                task:        'text',
                synced_at:   'date'
            },
            url: 'http://offline-sync.node/api/public/attachments'
        }
    }
});