var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var bower     = require('gulp-bower');
var inject    = require("gulp-inject");

var paths = {
  sass: ['./scss/**/*.scss'],
  modules: ['./www/lib/modules/']
};


var modules = [
    // 'www/lib/modules/persistence/lib/persistence.js'
    //,'www/lib/modules/persistence/lib/persistence.store.sql.js'
    //,'www/lib/modules/persistence/lib/persistence.store.websql.js'
    //,'www/lib/modules/persistence/lib/persistence.store.cordovasql.js'
    //,'www/lib/modules/angular-webstorage/angular-webstorage.js'
     'www/lib/modules/zepto/zepto.js'
    ,'www/lib/ionic/js/angular/angular-animate.js'
    ,'www/lib/modules/filesize/lib/filesize.js'
    ,'www/lib/modules/momentjs/moment.js'
    ,'www/lib/modules/angular-moment/angular-moment.js'
    ,'www/lib/modules/cordova-simplefilemanagement/painlessfs.js'
    //,'www/lib/modules/angular-phonegap-ready/ready.js'
    //,'www/lib/modules/angular-phonegap-filesystem/filesystem.js'
    ];

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});


gulp.task('copy', function(){
  gulp.src(['./plugins/com.phonegap.plugins.sqlite/www/SQLitePlugin.js',
            './plugins/com.phonegap.plugins.sqlite/Lawnchair-adapter/Lawnchair-sqlitePlugin.js'])
    .pipe(gulp.dest('www/lib/modules/lawnchair-adapter/'));
});


gulp.task('install', function() {
  bower()
    .pipe(gulp.dest('www/lib/modules/'))
});

gulp.task('inject', function() {
  gulp.src('./www/index.html')
  .pipe(inject(gulp.src(modules , {
    read: false,
    addRootSlash: false
    }),{
      transform: function (filepath, file, i, length) {
      newFilePath = filepath.replace('/www/','');
      return '<script src="'+newFilePath+'"></script>';
      }
    })) // Not necessary to read the files (will speed up things), we're only after their paths
  .pipe(gulp.dest("./www/"));
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
});

gulp.task('default', ['sass', 'install' , 'copy', 'inject']);
