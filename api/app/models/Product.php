<?php

class Product extends \Eloquent
{
    protected $fillable = ['name','title'];

    /**
    * The database table used by the model.
    *
    * @var string
    */
    protected $table = 'products';

    public function steps()
    {
        return $this->hasMany('Step');
    }

    public function booklet()
    {
        return $this->belongsTo('Media');
    }

    public function demo()
    {
        return $this->belongsTo('Media');
    }

    public function image()
    {
        return $this->belongsTo('Media');
    }

    public function video()
    {
        return $this->belongsTo('Media');
    }


}
