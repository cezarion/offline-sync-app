angular.module('starter.services', [])

/**
 * A simple example service that returns some data.
 */
.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var friends = [
    { id: 0, name: 'Scruff McGruff' },
    { id: 1, name: 'G.I. Joe' },
    { id: 2, name: 'Miss Frizzle' },
    { id: 3, name: 'Ash Ketchum' }
  ];

  return {
    all: function() {
      return friends;
    },
    get: function(friendId) {
      // Simple index lookup
      return friends[friendId];
    }
  }
})
//Synchronisation
.factory('SyncData', function(DB_CONFIG, RestApi, ProductModel){

  var syncUrl = '';
  return {

    syncProducts: function(){
      syncUrl = DB_CONFIG.tables.products.url;
      RestApi.load(syncUrl).then(function(response)
      {
         angular.forEach(response.data , function(item){
           ProductModel.add(item);
         });
         console.log(ProductModel.all());
      });
      return 'Sync Products';
    }
  }

});
