<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("products", function($table) {
            $table->increments("id");
            $table->string("name", 320);
            $table->string("title", 320);
            $table->mediumText("content");
            $table->unsignedInteger('image_id');
            $table->unsignedInteger('demo_id');
            $table->unsignedInteger('booklet_id');
            $table->unsignedInteger('video_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }

}
