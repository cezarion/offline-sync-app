<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("products", function($table)
        {
            $table->foreign('image_id')->references('id')->on('medias')->onDelete('cascade');
            $table->foreign('demo_id')->references('id')->on('medias')->onDelete('cascade');
            $table->foreign('booklet_id')->references('id')->on('medias')->onDelete('cascade');
            $table->foreign('video_id')->references('id')->on('medias')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function($table)
        {
            $table->dropForeign('products_image_id_foreign');
            $table->dropForeign('products_demo_id_foreign');
            $table->dropForeign('products_booklet_id_foreign');
            $table->dropForeign('products_video_id_foreign');
        });
    }

}
