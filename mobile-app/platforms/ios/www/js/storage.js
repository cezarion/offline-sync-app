/*
* @Author: Cezarion
* @Date:   2014-05-07 15:45:25
* @Last Modified by:   Cezarion
* @Last Modified time: 2014-05-14 18:13:00
*/

angular.module('starter.storage', ['starter.config'])
.factory('initStorage' , function($rootScope, APP_CONFIG){

    $rootScope.apiBase = APP_CONFIG.api_base;
    $rootScope.store = Lawnchair({
        adapters: ['dom', 'webkit-sqlite'],
        name: 'clientDb',
        table: 'medias'
      }, function(e){
      console.log('Storage open');
    });

    //return $rootScope;
})
.factory('remoteResources', function($rootScope, $http){
    var resource = {};

    resource.checkForUpdates = function(){
        // Check our last updated date on the device
        $rootScope.store.get('last_sync', function(last_sync){
            last_local_update = last_sync.value;
            $http({
                method: 'GET',
                url: $rootScope.api_base + '/attachments'//updates/last?' + new Date().getTime()
            })
            .success(function(data){
                var last_remote_update = data.date;
                // Check if remote info is fresher than local
                if(last_remote_update > last_local_update || last_local_update === null){
                    // Run resource functions
                    resource.getInformation();
                    // Update last update date locally
                    $rootScope.store.save({
                        key: 'last_sync',
                        value: new Date()
                    })
                }
            });
        });
    };

    resource.getInformation = function(){
      // Run a GET request against the information API call
      $http({
          method: 'GET',
          url: $rootScope.apiBase + '/attachments' //information?'+new Date().getTime()
      }).
      success(
          function(data){
              //console.log(data);
              //Store the retrieved information with a set key
              $rootScope.store.save({
                  key : data.id,
                  value : data
              });
          }
      );
  };

  return resource;

});

// return {
//     all: function() {
//       return $http.get('http://offline-sync.node/api/public/products').then(function(response) {
//         products = response.data;
//         $rootScope.$broadcast('handleSharedProducts',products);
//         return products;
//       })
//     },
//     get: function (productId)
//     {
//       return $http.get('http://offline-sync.node/api/public/products/'+ productId ).then(function(response) {
//         product = response.data;
//         $rootScope.$broadcast('handleSharedProducts',product);
//         return product;
//       })
//     }
//   };