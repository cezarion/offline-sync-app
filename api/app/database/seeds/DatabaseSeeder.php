<?php

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $this->call('AppUserTableSeeder');
        $this->call('MediaTableSeeder');
        $this->call('ProductTableSeeder');
        $this->call('StepTableSeeder');
    }

}

class MediaTableSeeder extends Seeder {

    public function run()
    {
        $count = 40;

        $this->command->info('Deleting existing Media table ...');

        DB::table('medias')->delete();

        if( Config::get('database.default') === 'mysql' ) {
            DB::unprepared("ALTER TABLE medias AUTO_INCREMENT = 1;");
        } else {
            DB::unprepared("UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'medias';");
        }

        $faker = Faker\Factory::create('fr_FR');

        $faker->addProvider(new Faker\Provider\File($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));

        $this->command->info('Inserting '.$count.' sample records using Faker ...');
        // $faker->seed(1234);

        for( $x=0 ; $x<$count; $x++ )
        {

            if( $x < 10)
                $files = glob(public_path().'/uploads/booklets/*.pdf');

            if( $x >= 10 && $x < 20)
                $files = glob(public_path().'/uploads/demos/*.mp4');

            if( $x >= 20 && $x < 30)
                $files = glob(public_path().'/uploads/images/*.jpg');

            if( $x >= 30 && $x < 40)
                $files = glob(public_path().'/uploads/videos/*.mp4');

            $file = $files[rand(0,2)];
            $fileinfo = pathinfo($file);
            $finfo = finfo_open(FILEINFO_MIME_TYPE);

            Media::create(array(
                'filename' => $fileinfo['filename'],
                'filesize' => filesize($file),
                'mimetype' => finfo_file($finfo, $file),
                'name' => $faker->sentence($nbWords = 6),
                'description' => $faker->sentence($nbWords = 20),
            ));
        }

        $this->command->info('Videos table seeded using Faker ...');
    }

}


class ProductTableSeeder extends Seeder {

    public function run()
    {
        $count = 20;

        $this->command->info('Deleting existing Products table ...');

        DB::table('products')->delete();

        if( Config::get('database.default') === 'mysql' ) {
            DB::unprepared("ALTER TABLE products AUTO_INCREMENT = 1;");
        } else {
            DB::unprepared("UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'products';");
        }

        $faker = Faker\Factory::create('fr_FR');

        $populator = new \Faker\ORM\Propel\Populator($faker);

        $this->command->info('Inserting '.$count.' sample records using Faker ...');
        // $faker->seed(1234);

        for( $x=0 ; $x<$count; $x++ )
        {

            Product::create(array(
                'name'       =>  $faker->sentence($nbWords = 6),
                'title'      =>  $faker->sentence($nbWords = 12),
                'content'    =>  $faker->paragraph(3),
                'image_id'   =>  $faker->randomNumber(21,30),
                'video_id'   =>  $faker->randomNumber(31,40),
                'demo_id'    =>  $faker->randomNumber(11,20),
                'booklet_id' =>  $faker->randomNumber(1,10),
            ));
        }

        $this->command->info('Images table seeded using Faker ...');
    }

}


class AppUserTableSeeder extends Seeder {

    public function run()
    {
        $count = 2;

        $this->command->info('Deleting existing appUsers table ...');
        DB::table('appUsers')->delete();

        if( Config::get('database.default') === 'mysql' ) {
            DB::unprepared("ALTER TABLE appUsers AUTO_INCREMENT = 1;");
        } else {
            DB::unprepared("UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = 'appUsers';");
        }

        $faker = Faker\Factory::create('fr_FR');

        $this->command->info('Inserting '.$count.' sample records using Faker ...');
        // $faker->seed(1234);

        for( $x=0 ; $x<$count; $x++ )
        {
            AppUser::create(array(
                'name'      => $faker->name,
                'email'     =>  $faker->email,
                'appId'     =>  $faker->randomNumber(5),
            ));
        }

        $this->command->info('Steps table seeded using Faker ...');
    }

}

class StepTableSeeder extends Seeder {

    public function run()
    {
        $count = 10;

        $this->command->info('Deleting existing Steps table ...');
        DB::table('steps')->delete();

        $faker = Faker\Factory::create('fr_FR');

        $faker->addProvider(new Faker\Provider\Lorem($faker));

        $this->command->info('Inserting '.$count.' sample records using Faker ...');
        // $faker->seed(1234);

        for( $x=0 ; $x<$count; $x++ )
        {
            Step::create(array(
                'title' => $faker->sentence($nbWords = 6),
                'description' => '<p>'.  implode('</p><p>', $faker->paragraphs(1)) .'</p>',
                'product_id' =>  $faker->randomNumber(1,20),
                'product_id' =>  $faker->randomNumber(1,20),
            ));
        }

        $this->command->info('Steps table seeded using Faker ...');
    }

}