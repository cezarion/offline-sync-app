<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppUserTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("appUsers", function($table) {
            $table->increments("id");
            $table->string("name", 320);
            $table->string("email", 320);
            $table->string("appId", 320);
            $table->timestamp("lastSyncDate");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appUsers');
    }

}
