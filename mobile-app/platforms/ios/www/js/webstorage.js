/*
* @Author: Cezarion
* @Date:   2014-05-14 18:43:12
* @Last Modified by:   Cezarion
* @Last Modified time: 2014-05-23 16:02:33
*/

angular.module('starter.webstorage', ['starter.config', 'angularMoment'])
.factory('DB' , function($q, $window, $log, APP_CONFIG){
    var self = this;
    self.db = null;

    self.init = function() {
        // Use self.db = window.sqlitePlugin.openDatabase({name: DB_CONFIG.name}); in production
        self.db = window.openDatabase(APP_CONFIG.dbName, APP_CONFIG.version, 'database' , 1000000);
        self.createTable();
    };

    self.columnsName = function( table ){
        var columns = {names:[], prepare:[]};
        angular.forEach(table.columns, function(param, index) {
                columns.names.push(index);
                columns.prepare.push('?');
            });
        return columns;
    }

    self.query = function(query, bindings) {
        bindings = typeof bindings !== 'undefined' ? bindings : [];
        var deferred = $q.defer();

        self.db.transaction(function(transaction) {
            transaction.executeSql(query, bindings, function(transaction, result) {
                deferred.resolve(result);
                console.info('SQL OK', query);
            }, function(transaction, error) {
                deferred.reject('SQL ERROR');
                console.error(error,query,bindings);
            });
        });
        return deferred.promise;
    };

    self.createTable = function(){
        angular.forEach(APP_CONFIG.tables, function(table) {
            var columns = [];

            angular.forEach(table.columns, function(param,name) {
                columns.push(name + ' ' + param);
            });

            var query = 'CREATE TABLE IF NOT EXISTS ' + table.name + ' (' + columns.join(',') + ')';
            self.query(query);

            //$log.debug('Query ' + query);
            //$log.debug('Table ' + table.name + ' initialized');
        });
    }

    self.dropTable = function(){
        angular.forEach(APP_CONFIG.tables, function(table) {

            var query = 'DROP TABLE IF EXISTS ' + table.name;
            self.query(query);

            //$log.debug('Query ' + query);
            //$log.debug('Table ' + table.name + ' initialized');
        });
    }

    self.fetchAll = function(result) {
        var output = [];

        for (var i = 0; i < result.rows.length; i++) {
            output.push(result.rows.item(i));
        }

        return output;
    };

    self.fetch = function(result) {
        return result.rows.item(0);
    };

    return self;
})

//Get datas
.factory('RestApi', function($log, $http, APP_CONFIG)
{
    var syncMedias  = APP_CONFIG.api_base + '/attachments';

    return {
        getMedias : function(params){
            params = typeof params !== 'undefined' ? params : {};
            var promise = $http({
                 url: syncMedias,
                 method: "GET",
                 params: params
             })
            .then(function(response){
                return response.data;
            }, function(error){
                $log.error('#LOG : Error while get attachments' + error);
                return false
            });
            return promise;
        }
    }
})

.factory('MediasManager' , function( $q, $log , $rootScope, $window, APP_CONFIG ){
    var appDirName = "offline-sync";
    var appDirPath = appDirName+"/";
    var fileName;

    return {
        download: function( fileurl ) {
            var b = new FileManager();
            fileName=fileurl.substr(fileurl.lastIndexOf('/')+1);
            b.remove_file(appDirPath,fileName, Log('complete delte'), Log('delete fail'));
            b.download_file(fileurl,appDirPath,fileName,Log('downloaded sucess'));
        },

        listFiles: function(){
            var deferred = $q.defer();

            var a = new DirManager();
            a.list(appDirName , function(result){
                deferred.resolve(result);
            });
            return deferred.promise;
        },

        list: function(){
            this.listFiles().then(function(results){
                console.log(results);
                return results;
            }, function(error) {
                deferred.reject('FILE LIST ERROR');
                console.error(error);
            });
        }
    }


})

// Resource service example
.factory('Medias', function($log, $q, DB, RestApi, APP_CONFIG) {
    var self = this,
        table = APP_CONFIG.tables.medias,
        messages = [],
        loading = false;
    //return {

        self.initialize = function(){
            self.loading = loading;
            self.messages = messages;
            self.lastSync();
        };


        self.reset = function(){
            DB.dropTable();
            DB.init();
        };

        self.all = function() {
            return DB.query('SELECT * FROM medias')
            .then(function(results){
                return DB.fetchAll(results);
            }, function(error){
                $log.error(error);
            });
        };

        self.getById = function(id) {
            return DB.query('SELECT * FROM medias WHERE id = ?', [id])
            .then(function(result){
                return DB.fetch(result);
            });
        };

        self.lastSync = function() {
            var self = this;
            self.getLastSync(function(lastSync){
                return lastSync;
            })
        };

        self.getLastSync = function(callback) {
            var self = this,
                syncDate;
            return DB.query('SELECT MAX(synced_at) as syncDate FROM syncHistory where task = "medias"')
            .then(function(results){
                syncDate = results.rows.item(0).syncDate;

                if( syncDate !== null ) {
                    self.userLog('Last local changes is ' + lastSync);
                    var lastSync = syncDate;
                } else {
                    self.userLog('Database is empty');
                    var lastSync = '1970-01-01 00:00:00';
                }

                callback(lastSync);
            });
        };

        self.sync = function(callback) {

            var self = this;
            $log.info('Starting database synchronization...');
            self.showLoader(true);
            self.userLog('Starting database synchronization...');

            this.getLastSync(function(lastSync){
                self.getChanges(lastSync, function(changes){
                    if( changes.length > 0 ) {
                        self.applyChanges(changes, callback);
                    } else {
                        self.userLog('Nothing to synchronize, everything is up to date');
                        self.showLoader(false);
                    }
                });
            })
        };

        self.getChanges = function(modifiedSince,callback) {
            var self = this,
                deferred = $q.defer(),
                data = {},
                params = modifiedSince !== null ? { 'updated-since' : modifiedSince } : '';

            RestApi.getMedias(params)
                   .then(function(data){
                        $log.debug('#Nb items : ' + data.length);
                        self.userLog('There are ' + data.length + ' items to sync');
                        self.userLog('It will ' + self.getDownloadSize(data) + ' to download');

                        callback(data);
                        deferred.resolve(data)
                        //return data
                    }, function(error){
                        $log.error('#ERROR : get medias failed ('+modifiedSince+')');
                        self.userLog('An error occur when trying to get medias from server');
                        callback();
                        deferred.reject(error);
                    });

            return deferred.promise;
        };

        self.userLog = function(message) {
            messages.push(message);
            this.messages = messages;
            $log.info(message);
            return this.messages;
        };

        self.clear = function() {
            messages = [];
            this.messages = messages;
            return this.messages;
        };

        self.showLoader = function( status ) {
            this.loading = status;
            $log.info(status);
            return this.loading;
        };

        self.applyChanges = function(medias, callback) {
            var self = this,
                sql = '',
                l = medias.length,
                i = 0,
                syncItems = { success:[] , errors: [] },
                now = moment().format('YYYY-MM-DD HH:mm:ss');

            columns = DB.columnsName(table);

            sql = "INSERT INTO medias ("+columns.names.join(',')+") " +
                    "VALUES ("+columns.prepare.join(',')+")";

            $log.info('Inserting or Updating in local database:');

            angular.forEach(medias,function(e){
                var values = [e.id,e.filename,e.filesize,e.mimetype,e.name,e.description,e.created_at,e.updated_at],
                    message = '';

                DB.query(sql,values).then(function(success){
                    message = i+'/'+ l + ' : Database Sync Ok : ' + values.join(' \n ');
                    self.userLog( message );
                    syncItems.success.push(e);
                    i++;
                }, function(error){
                    message = i+'/'+ l + ' : Database Sync KO : ' + values.join(' \n ');
                    self.userLog( message );
                    syncItems.errors.push(e);
                    i++;
                });
            });

            sql = "INSERT INTO syncHistory (task, synced_at) VALUES ('medias', '"+now+"')";
            DB.query(sql).then(function(success){
                self.userLog('Last sync is set at : ' + now);
                self.showLoader(false);
            });
        };

        self.getDownloadSize = function(medias,callback) {
            var size = 0;
            angular.forEach(medias, function(media){
                size += +media.filesize;
            });
            return filesize(size);
        };

        return self;
    //}
});